﻿/*
 Name:		CarComfortModule.ino
 Created:	10/18/2016 1:55:33 PM
 Author:	Łukasz Wal
*/

#include "DHT.h"
#define DHTPIN 5
#define DHTTYPE DHT11

DHT dht(DHTPIN, DHTTYPE);


int signalPilotClose = 3;
int signalPilotOpen = 7;
int signalPilotCloseStatusCount = 0;


int funFollowMeHome = 9;
int funWindowsUp = 10;
int funWindowsDownTemp = 11;
int funWindowsLower = 12;

int timeLight = 0;
int timeWindowsUP = 0;
int timeWindowsDownTemp = 0;
int timeWindowsLower = 0;
int timeForCloseOpenClose = 0;

int windowsUPPIN = 4; //czerwona
int windowsDownTempPIN = 6; //żółta
int lightsPIN = 2; //zielona
int windowsLowerPIN = 8; //niebieska




// the setup function runs once when you press reset or power the board
void setup() {
	Serial.begin(9600);
	pinMode(lightsPIN, OUTPUT);
	pinMode(windowsDownTempPIN, OUTPUT);
	pinMode(windowsUPPIN, OUTPUT);
	pinMode(windowsLowerPIN, OUTPUT);

	pinMode(funFollowMeHome, INPUT); //dip1,
	pinMode(funWindowsUp, INPUT); //dip2, 
	pinMode(funWindowsDownTemp, INPUT); //dip3,  
	pinMode(funWindowsLower, INPUT);

	dht.begin();
}

// the loop function runs over and over again until power down or reset
void loop() {
	float temp = dht.readTemperature();
	//float hum = dht.readHumidity();
	
	/*Serial.print(signalPilotCloseStatusCount);
	Serial.print(" : ");
	Serial.print(timeForCloseOpenClose);
	Serial.print(" : ");
	Serial.print(timeWindowsLower);
	Serial.println();*/

	int lightingTime = analogRead(A1) * 20;
	int lightSensor = analogRead(A0); //* 1.2;
	int lightMeter = analogRead(A2);

	Serial.print(lightSensor);
	Serial.print(" : ");
	Serial.print(lightMeter);
	Serial.println();

	if (isnan(temp)) {
		Serial.println("Blad czujnika");
	}
	else
	{
		/*Serial.print("Wilgotnosc: ");
		Serial.print(hum);
		Serial.print(" % ");*/
		/*Serial.print("Temperatura: ");
		Serial.print(temp);
		Serial.println(" *C");*/
	}

	//Serial.println(timeLight);
	/*Serial.print("Czujnik: ");
	Serial.print(lightSensor);

	Serial.print(" Pot: ");
	Serial.print(lightMeter);
	Serial.println();*/


	if (digitalRead(signalPilotClose) == HIGH && digitalRead(funFollowMeHome) == HIGH && lightSensor < lightMeter) {
		// followMeHome
		timeLight = lightingTime;
	}
	if (digitalRead(signalPilotClose) == HIGH && digitalRead(funWindowsUp) == HIGH) timeWindowsUP = lightingTime; // domykanie szyb
	if (digitalRead(signalPilotClose) == HIGH && digitalRead(funWindowsDownTemp) == HIGH && temp > 18.0) timeWindowsDownTemp = 2000; //Odsuwanie gdy temp
	if (digitalRead(signalPilotClose) == HIGH && signalPilotCloseStatusCount == 3 && digitalRead(funWindowsLower) == HIGH && timeForCloseOpenClose > 0) {
		//uchylanie
		timeWindowsLower = 1000;
	}

	if (digitalRead(signalPilotClose) == HIGH && signalPilotCloseStatusCount == 0) {
		signalPilotCloseStatusCount = 1;
		timeForCloseOpenClose = 4000;
	}

	if (digitalRead(signalPilotClose) == HIGH && signalPilotCloseStatusCount == 2) {
		signalPilotCloseStatusCount = 3;
	}

	if (digitalRead(signalPilotOpen) == HIGH && signalPilotCloseStatusCount == 1) {
		signalPilotCloseStatusCount = 2;
	}

	if (timeForCloseOpenClose > 0) {
		timeForCloseOpenClose--;
	}
	else
	{
		signalPilotCloseStatusCount = 0;
	}

	if (timeWindowsLower > 0) {
		digitalWrite(windowsLowerPIN, HIGH);
		timeWindowsLower--;
		if (timeWindowsLower == 0) {
			signalPilotCloseStatusCount = 0;
		}
	}
	else
	{
		digitalWrite(windowsLowerPIN, LOW);
	}


#pragma region FollowMeHome

	if (timeLight > 0) {
		digitalWrite(lightsPIN, HIGH);
		timeLight--;
	}
	else
	{
		digitalWrite(lightsPIN, LOW);
	}
#pragma endregion

#pragma region WindowsUP

	if (timeWindowsUP > 0) {
		digitalWrite(windowsUPPIN, HIGH);
		timeWindowsUP--;
	}
	else
	{
		digitalWrite(windowsUPPIN, LOW);
	}
#pragma endregion

#pragma region WindowsDownTemp

	if (timeWindowsDownTemp > 0) {
		digitalWrite(windowsDownTempPIN, HIGH);
		timeWindowsDownTemp--;
	}
	else
	{
		digitalWrite(windowsDownTempPIN, LOW);
	}

#pragma endregion

	delay(1);
}
